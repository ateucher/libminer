
<!-- README.md is generated from README.Rmd. Please edit that file -->

# libminer

<!-- badges: start -->
<!-- badges: end -->

The goal of libminer is to provide an overview of your R library setup.
It is a toy package from a workshop and not meant for serious use.

## Installation

You can install the development version of libminer like so:

``` r
devtools::install_gitlab("ateucher/libminer")
```

## Example

This is a basic example which shows you how to solve a common problem:

``` r
library(libminer)

lib_summary()
#>                                                                                        Library
#> 1                         /Library/Frameworks/R.framework/Versions/4.3-arm64/Resources/library
#> 2 /private/var/folders/_f/n9fw7ctx3fqf2ty9ylw502g80000gn/T/Rtmp1xveV2/temp_libpath6a0439087ad3
#> 3                                                      /Users/andy/Library/R/arm64/4.3/library
#>   n_packages
#> 1         29
#> 2          1
#> 3        415

# Or to calculate sizes:

lib_summary(sizes = TRUE)
#>                                                                                        Library
#> 1                         /Library/Frameworks/R.framework/Versions/4.3-arm64/Resources/library
#> 2 /private/var/folders/_f/n9fw7ctx3fqf2ty9ylw502g80000gn/T/Rtmp1xveV2/temp_libpath6a0439087ad3
#> 3                                                      /Users/andy/Library/R/arm64/4.3/library
#>   n_packages   lib_size
#> 1         29   71828984
#> 2          1      14175
#> 3        415 3069030373
```
